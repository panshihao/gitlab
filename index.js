/* 
* @Author: Marte
* @Date:   2017-03-28 19:54:04
* @Last Modified by:   Marte
* @Last Modified time: 2017-03-28 19:54:51
edit11
edit22
*/

$(document).ready(function(){
    var myChart = echarts.init(document.getElementById("fenleishujv"));
        // 南丁格尔饼状图
        myChart.setOption({
        backgroundColor: 'rgba(0, 0, 0, 0.5)',
        title : {
        text: '南丁格尔玫瑰图',
        textStyle: {
            color: '#ccc'
        },
        subtext: '大河数据',
        x:'center'
        },
        tooltip : {
            trigger: 'item',
            formatter: "{a} <br/>{b} : {c} ({d}%)"
        },
        legend: {
            x : 'center',
            y : 'bottom',
            data:['rose1','rose2','rose3','rose4','rose5','rose6','rose7'],
            textStyle: {
            color: '#ccc'
            }
        },
        calculable : true,
        series :{
            name:'面积模式',
            type:'pie',
            radius : [30, 110],
            center : ['50%', '50%'],
            roseType : 'area',
            data:[
                {value:10, name:'rose1'},
                {value:5,  name:'rose2'},
                {value:15, name:'rose3'},
                {value:25, name:'rose4'},
                {value:20, name:'rose5'},
                {value:35, name:'rose6'},
                {value:30, name:'rose7'}
            ]
        },
        // 阴影的配置
        itemStyle: {
         normal: {
            // 阴影的大小
            shadowBlur: 100,
            // 阴影水平方向上的偏移
            shadowOffsetX: 0,
            // 阴影垂直方向上的偏移
            shadowOffsetY: 0,
            // 阴影颜色
            shadowColor: 'rgba(0, 0, 0, 0.5)'
            },
        // hover上之后的效果
        emphasis: {
            shadowBlur: 100,
            shadowColor: 'rgba(255, 199, 199, 1)'
            }
        },
        // 颜色的配置
        color: ['#08419f','#0991dc','#00ccf1','#3de5c7','#ed9f4a','#f2e019','#ffffff']
    });
});
